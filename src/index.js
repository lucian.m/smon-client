'use strict';

module.exports = process.platform === 'win32'? require('./windows/index.js') : /* otherwise */ require('./linux/PluginManager.js');
