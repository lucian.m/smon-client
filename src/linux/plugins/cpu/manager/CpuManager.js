var _ = require('lodash');
var fs = require('fs');

var Manager = require('../manager/Manager');
var CpuModel = require('../model/Cpu');
var logger = require('../../../../../log');

var _cache = {
        'processors': {

        }
    };

function CpuManager() {
    this.processors = {'primary': null, 'cores': []};
}

CpuManager.prototype.getProcessors = function () {
    logger.log('debug', 'getProcessors');
    var self = this;
    fs.readFile('/proc/stat', function (err, buf) {
        if (err){
            self.emit('error', {'type': 'proc', err: err});
        } else {
            var lines = buf.toString().trim().split("\n");
            lines.forEach(function (l) {
                var p = l.indexOf(' ');
                var k = l.substr(0, p);
                var v = l.substr(p).trim();
                if (k.indexOf('cpu') === 0) {
                    //agregate processor
                    if('cpu' === k) {
                        if(self.processors.primary === null) {
                            cpu = new CpuModel(k, true);
                            cpu.updateProc(v.split(' '));
                            self.processors.primary = cpu;
                        } else {
                            self.processors.primary.updateProc(v.split(' '));
                        }
                    } else {
                        var cpu = _.find(self.processors.cores,  function(chr) {
                            return chr.model.id === k;
                        });
                        if(!cpu) {
                            cpu = new CpuModel(k, false);
                            cpu.updateProc(v.split(' '));
                            self.processors.cores.push(cpu);
                        } else {
                            cpu.updateProc(v.split(' '));
                        }
                    }
                }
            });
            
            self.emit('get:stats', self.processors);
        }
    });
};

CpuManager.prototype.runStats = function(time) {
    var self = this;
    var int = setInterval(interval, time);
    function interval() {
        self.getProcessors();
    }
    
    return int;
};

CpuManager.prototype.__proto__ = Manager.prototype;

module.exports = CpuManager;