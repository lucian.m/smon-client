var shell = require('../utils/utils.js').shell;
var _ = require("lodash");
var fs = require('fs');
var os = require('os');

var Manager = require('../manager/Manager');
var AdapterModel = require('../model/Adapter');
var logger = require('../../../log');

function NetworkManager() {
    this.adapters = {};
    this.policies = {'adapter_download': {}, 'adapter_upload': {}};
    this.statistics = {
        'date': Date.now(),
        'data': {
            'date_start': Date.now(),
            'date_end': Date.now(),
            adapters: {}
        }
    }
    this.init = false;
};

NetworkManager.prototype = {
    getAdapters: function () {
        logger.log('debug', 'getAdapters', {plugin: 'network'});
        var nics = os.networkInterfaces();
        var self = this;
        for(var nic in nics) {
            if(!self.adapters[nic]) {
                self.adapters[nic] = new AdapterModel(nic);
            }
                        
            self.adapters[nic].update(nics[nic]);
        };
    },
    getStats: function () {
        logger.log('debug', 'getStats', {plugin: 'network'});
        var self = this;
        fs.readFile('/proc/net/dev', function (err, buf) {
            if (err) {
                self.emit('error', {'plugin': 'network', 'type': '/proc/net/dev', err: err});
            } else {
                var lines = buf.toString().trim().split("\n");
                lines.splice(0, 2);
                lines.forEach(function (str) {
                    var line = str.trim().split(/\s+/);
                    var nic = line[0].slice(0, -1);
                    if(self.adapters[nic]) {
                        self.adapters[nic].updateStats(line);
                    }
                });
                
                self.sendStats(self.adapters);
            }
        });
    },
    runStats: function (time) {
        var self = this;
        self.getAdapters();
        setInterval(interval, time);
        function interval() {
            self.getStats();
        }
    }
};

NetworkManager.prototype.sendStats = function(adapters) {
    if(!this.init) {
        this.init = true;
    } else {
        var data = {adapters: {}, time: Date.now()};
        for(var key in adapters) {
            data['adapters'][key] = adapters[key].getModel();
        }

        setTimeout(this.checkPolicies.bind(this), 3000);
        this.setStatistics(data.adapters);
        this.emit('get:stats', data);
    }
};

/*
 * Set the statistics, we send them every minute
 * We need to calculate each start and end of a minute 
 */
NetworkManager.prototype.setStatistics = function(adapters) {
    var self = this;
    self.statistics.date = new Date();
//    console.log(adapters['eth0']['stats']['live'])
    if(self.statistics.date.getSeconds() == 0 && !self.statistics.init) {
        self.statistics.init = true;
        self.statistics.data.increments = 0;
        self.statistics.data.date_start = Date.now();
        for(var key in adapters) {
            self.statistics.data.adapters[key] = {
                'adapter': key,
                'receive': {
                    'high_bytes': adapters[key]['stats']['live']['receive']['bytes'],
                    'avg_bytes': 0,
                    'low_bytes': adapters[key]['stats']['live']['receive']['bytes'],
                    'total_bytes': 0,
                    'high_packets': adapters[key]['stats']['live']['receive']['packets'],
                    'avg_packets': 0,
                    'low_packets': adapters[key]['stats']['live']['receive']['packets'],
                    'total_packets': 0,
                    'high_errs': adapters[key]['stats']['live']['receive']['errs'],
                    'avg_errs': 0,
                    'low_errs': adapters[key]['stats']['live']['receive']['errs'],
                    'total_errs': 0,
//                    'high_drop': adapters[key]['receive']['drop'],
//                    'avg_drop': 0,
//                    'low_drop': adapters[key]['receive']['drop'],
//                    'total_drop': 0,
//                    'high_fifo': adapters[key]['receive']['fifo'],
//                    'avg_fifo': 0,
//                    'low_fifo': adapters[key]['receive']['fifo'],
//                    'total_fifo': 0,
//                    'high_frame': adapters[key]['receive']['frame'],
//                    'avg_frame': 0,
//                    'low_frame': adapters[key]['receive']['frame'],
//                    'total_frame': 0,
//                    'high_compressed': adapters[key]['receive']['compressed'],
//                    'avg_compressed': 0,
//                    'low_compressed': adapters[key]['receive']['compressed'],
//                    'total_compressed': 0,
//                    'high_multicast': adapters[key]['receive']['multicast'],
//                    'avg_multicast': 0,
//                    'low_multicast': adapters[key]['receive']['multicast'],
//                    'total_multicast': 0
                },
                'transmit': {
                    'high_bytes': adapters[key]['stats']['live']['transmit']['bytes'],
                    'avg_bytes': 0,
                    'low_bytes': adapters[key]['stats']['live']['transmit']['bytes'],
                    'total_bytes': 0,
                    'high_packets': adapters[key]['stats']['live']['transmit']['packets'],
                    'avg_packets': 0,
                    'low_packets': adapters[key]['stats']['live']['transmit']['packets'],
                    'total_packets': 0,
                    'high_errs': adapters[key]['stats']['live']['transmit']['errs'],
                    'avg_errs': 0,
                    'low_errs': adapters[key]['stats']['live']['transmit']['errs'],
                    'total_errs': 0,
//                    'high_drop': adapters[key]['transmit']['drop'],
//                    'avg_drop': 0,
//                    'low_drop': adapters[key]['transmit']['drop'],
//                    'total_drop': 0,
//                    'high_fifo': adapters[key]['transmit']['fifo'],
//                    'avg_fifo': 0,
//                    'low_fifo': adapters[key]['transmit']['fifo'],
//                    'total_fifo': 0,
//                    'high_frame': adapters[key]['transmit']['frame'],
//                    'avg_frame': 0,
//                    'low_frame': adapters[key]['transmit']['frame'],
//                    'total_frame': 0,
//                    'high_compressed': adapters[key]['transmit']['compressed'],
//                    'avg_compressed': 0,
//                    'low_compressed': adapters[key]['transmit']['compressed'],
//                    'total_compressed': 0,
//                    'high_multicast': adapters[key]['transmit']['multicast'],
//                    'avg_multicast': 0,
//                    'low_multicast': adapters[key]['transmit']['multicast'],
//                    'total_multicast': 0
                }
            }
        }
    }
    if(self.statistics.init) {
        for(key in self.statistics.data.adapters) {
            /*
             * receive
             */
            if(self.statistics.data.adapters[key]['receive']['high_bytes'] < adapters[key]['stats']['live']['receive']['bytes']) {
                self.statistics.data.adapters[key]['receive']['high_bytes'] = adapters[key]['stats']['live']['receive']['bytes'];
            }
            if(self.statistics.data.adapters[key]['receive']['high_packets'] < adapters[key]['stats']['live']['receive']['packets']) {
                self.statistics.data.adapters[key]['receive']['high_packets'] = adapters[key]['stats']['live']['receive']['packets'];
            }
            if(self.statistics.data.adapters[key]['receive']['high_errs'] < adapters[key]['stats']['live']['receive']['errs']) {
                self.statistics.data.adapters[key]['receive']['high_errs'] = adapters[key]['stats']['live']['receive']['errs'];
            }
            
            if(self.statistics.data.adapters[key]['receive']['low_bytes'] > adapters[key]['stats']['live']['receive']['bytes']) {
                self.statistics.data.adapters[key]['receive']['low_bytes'] = adapters[key]['stats']['live']['receive']['bytes'];
            }
            if(self.statistics.data.adapters[key]['receive']['low_packets'] > adapters[key]['stats']['live']['receive']['packets']) {
                self.statistics.data.adapters[key]['receive']['low_packets'] = adapters[key]['stats']['live']['receive']['packets'];
            }
            if(self.statistics.data.adapters[key]['receive']['low_errs'] > adapters[key]['stats']['live']['receive']['errs']) {
                self.statistics.data.adapters[key]['receive']['low_errs'] = adapters[key]['stats']['live']['receive']['errs'];
            }
            
            self.statistics.data.adapters[key]['receive']['total_bytes'] = self.statistics.data.adapters[key]['receive']['total_bytes'] + parseFloat(adapters[key]['stats']['live']['receive']['bytes']);
            self.statistics.data.adapters[key]['receive']['total_packets'] = self.statistics.data.adapters[key]['receive']['total_packets'] + parseFloat(adapters[key]['stats']['live']['receive']['packets']);
            self.statistics.data.adapters[key]['receive']['total_errs'] = self.statistics.data.adapters[key]['receive']['total_errs'] + parseFloat(adapters[key]['stats']['live']['receive']['errs']);
            
            /*
             * transmit
             */
            if(self.statistics.data.adapters[key]['transmit']['high_bytes'] < adapters[key]['stats']['live']['transmit']['bytes']) {
                self.statistics.data.adapters[key]['transmit']['high_bytes'] = adapters[key]['stats']['live']['transmit']['bytes'];
            }
            if(self.statistics.data.adapters[key]['transmit']['high_packets'] < adapters[key]['stats']['live']['transmit']['packets']) {
                self.statistics.data.adapters[key]['transmit']['high_packets'] = adapters[key]['stats']['live']['transmit']['packets'];
            }
            if(self.statistics.data.adapters[key]['transmit']['high_errs'] < adapters[key]['stats']['live']['transmit']['errs']) {
                self.statistics.data.adapters[key]['transmit']['high_errs'] = adapters[key]['stats']['live']['transmit']['errs'];
            }
            
            if(self.statistics.data.adapters[key]['transmit']['low_bytes'] > adapters[key]['stats']['live']['transmit']['bytes']) {
                self.statistics.data.adapters[key]['transmit']['low_bytes'] = adapters[key]['stats']['live']['transmit']['bytes'];
            }
            if(self.statistics.data.adapters[key]['transmit']['low_packets'] > adapters[key]['stats']['live']['transmit']['packets']) {
                self.statistics.data.adapters[key]['transmit']['low_packets'] = adapters[key]['stats']['live']['transmit']['packets'];
            }
            if(self.statistics.data.adapters[key]['transmit']['low_errs'] > adapters[key]['stats']['live']['transmit']['errs']) {
                self.statistics.data.adapters[key]['transmit']['low_errs'] = adapters[key]['stats']['live']['transmit']['errs'];
            }
            
            self.statistics.data.adapters[key]['transmit']['total_bytes'] = self.statistics.data.adapters[key]['transmit']['total_bytes'] + parseFloat(adapters[key]['stats']['live']['transmit']['bytes']);
            self.statistics.data.adapters[key]['transmit']['total_packets'] = self.statistics.data.adapters[key]['transmit']['total_packets'] + parseFloat(adapters[key]['stats']['live']['transmit']['packets']);
            self.statistics.data.adapters[key]['transmit']['total_errs'] = self.statistics.data.adapters[key]['transmit']['total_errs'] + parseFloat(adapters[key]['stats']['live']['transmit']['errs']);
        }
//        console.log(self.statistics.data.adapters['eth0'].receive);
//        console.log(adapters['eth0']['stats']['live']['receive']['bytes']);
        self.statistics.data.increments++;
        if(self.statistics.date.getSeconds() == 59) {
            self.statistics.init = false;
            /*
             * we send the data now and we reset
             */
            self.statistics.data.date_end = Date.now();
            for(key in self.statistics.data.adapters) {
                self.statistics.data.adapters[key]['receive']['avg_bytes'] = _.round(self.statistics.data.adapters[key]['receive']['total_bytes'] / self.statistics.data.increments);
                self.statistics.data.adapters[key]['receive']['avg_packets'] = _.round(self.statistics.data.adapters[key]['receive']['total_packets'] / self.statistics.data.increments);
                self.statistics.data.adapters[key]['receive']['avg_errs'] = _.round(self.statistics.data.adapters[key]['receive']['total_errs'] / self.statistics.data.increments);

                self.statistics.data.adapters[key]['transmit']['avg_bytes'] = _.round(self.statistics.data.adapters[key]['transmit']['total_bytes'] / self.statistics.data.increments);
                self.statistics.data.adapters[key]['transmit']['avg_packets'] = _.round(self.statistics.data.adapters[key]['transmit']['total_packets'] / self.statistics.data.increments);
                self.statistics.data.adapters[key]['transmit']['avg_errs'] = _.round(self.statistics.data.adapters[key]['transmit']['total_errs'] / self.statistics.data.increments);
            }

            this.emit('get:statistics', self.statistics.data);
            self.statistics.data.adapters = {};
        }
    }
};
NetworkManager.prototype.setPolicies = function(policies, type) {
    var self = this;
    if(type === 'adapter_upload') {
        self.policies['adapter_upload'] = {};
    }
    if(type === 'adapter_download') {
        self.policies['adapter_download'] = {};
    }
    policies.forEach(function(policy) {
        if(!self.policies[policy.id]) {
            self.policies[policy.type][policy.id] = policy;
        } else {
            policy['status'] = self.policies[policy.type][policy.id]['status'];
            self.policies[policy.type][policy.id] = policy;
        }
    });
};

NetworkManager.prototype.checkPolicies = function() {
    var self = this;
    //upload
    for(var pid in self.policies['adapter_upload']) {
        if(self.adapters[self.policies['adapter_upload'][pid]['card_name']]) {
            var adapter = self.adapters[self.policies['adapter_upload'][pid]['card_name']].model.stats;
            var open = self.policies['adapter_upload'][pid]['upload_open'] * 1024 * 1024 / 8;
            var close = self.policies['adapter_upload'][pid]['upload_close']  * 1024 * 1024 / 8;
            if (adapter.live.transmit.bytes >= open && (self.policies['adapter_upload'][pid]['status'] === 'closed' || self.policies['adapter_upload'][pid]['status'] === 'not_verified')) {
                //open the policy and add the incident
                self.policies['adapter_upload'][pid]['status'] = 'opened';
                var adapterUpload = {'policy': self.policies['adapter_upload'][pid], 'data': self.adapters[self.policies['adapter_upload'][pid]['card_name']].getModel(), 'status': 'open', type: 'adapter_upload', time: Date.now()};
                self.emit('policy:send', adapterUpload);
            }
            if (adapter.live.transmit.bytes < close && (self.policies['adapter_upload'][pid]['status'] === 'opened' || self.policies['adapter_upload'][pid]['status'] === 'not_verified')) {
                //close the policy and close the incident
                self.policies['adapter_upload'][pid]['status'] = 'closed';
                var adapterUpload = {'policy': self.policies['adapter_upload'][pid], 'data': self.adapters[self.policies['adapter_upload'][pid]['card_name']].getModel(), 'status': 'close', type: 'adapter_upload', time: Date.now()};
                self.emit('policy:send', adapterUpload);
            }
        }
    }
    
    //download
    for(var pid in self.policies['adapter_download']) {
        if(self.adapters[self.policies['adapter_download'][pid]['card_name']]) {
            var adapter = self.adapters[self.policies['adapter_download'][pid]['card_name']].model.stats;
            var open = self.policies['adapter_download'][pid]['download_open'] * 1024 * 1024 / 8;
            var close = self.policies['adapter_download'][pid]['download_close']  * 1024 * 1024 / 8;
            if (adapter.live.receive.bytes >= open && (self.policies['adapter_download'][pid]['status'] === 'closed' || self.policies['adapter_download'][pid]['status'] === 'not_verified')) {
                //open the policy and add the incident
                self.policies['adapter_download'][pid]['status'] = 'opened';
                var adapterDownload = {'policy': self.policies['adapter_download'][pid], 'data': self.adapters[self.policies['adapter_download'][pid]['card_name']].getModel(), 'status': 'open', type: 'adapter_download', time: Date.now()};
                self.emit('policy:send', adapterDownload);
            }
            if (adapter.live.receive.bytes < close && (self.policies['adapter_download'][pid]['status'] === 'opened' || self.policies['adapter_download'][pid]['status'] === 'not_verified')) {
                //close the policy and close the incident
                self.policies['adapter_download'][pid]['status'] = 'closed';
                var adapterDownload = {'policy': self.policies['adapter_download'][pid], 'data': self.adapters[self.policies['adapter_download'][pid]['card_name']].getModel(), 'status': 'close', type: 'adapter_download', time: Date.now()};
                self.emit('policy:send', adapterDownload);
            }
        }
    }
};

NetworkManager.prototype.__proto__ = Manager.prototype;

module.exports = NetworkManager;