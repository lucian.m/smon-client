var shell = require('../utils/utils.js').shell;
var Manager = require('../manager/Manager');
var RamModel = require('../model/Ram');
var logger = require('../../../log');

function RamManager() {
    this.ramModel = new RamModel();
    this.policies = {};
    this.statistics = {
        date: new Date(),
        init: false,
        data: {
            mem: {
                percent: 0,
                used: 0,
                total: 0
            },
            swap: {
                percent: 0,
                used: 0,
                total: 0
            }
        }
    };
};

RamManager.prototype.getStats = function(){
        logger.log('debug', 'getStats', {plugin: 'ram'});
        var self = this;
        shell('free -b', []).fork(
            function (error) {
                self.emit('error', {'plugin':'ram', 'type': 'proc', err: error});
            },
            function (data) {
                self.ramModel.update(data.output);
                self.checkPolicies();
                self.setStatistics(self.ramModel.getModel());
                self.emit('get:stats', {data: self.ramModel.getModel(), time: Date.now()});
            }
        );
};

RamManager.prototype.runStats = function(time) {
    var self = this;
    var int = setInterval(interval, time);
    function interval() {
        self.getStats();
    }
    return int;
};

/*
 * Set the statistics, we send them every minute
 * We need to calculate each start and end of a minute 
 */
RamManager.prototype.setStatistics = function(stats) {
    var self = this;
    self.statistics.date = new Date();
    if(self.statistics.date.getSeconds() == 0 && !self.statistics.init) {
        self.statistics.init = true;
        self.statistics.data.date_start = Date.now();
    }
    if(self.statistics.init) {
        if(self.statistics.date.getSeconds() == 59) {
            self.statistics.init = false
            self.statistics.data.mem.percent = parseFloat(stats.usage.mem.percent);
            self.statistics.data.mem.used = parseFloat(stats.buffers.used);
            self.statistics.data.mem.total = parseFloat(stats.mem.total);
            self.statistics.data.swap.percent = parseFloat(stats.usage.swap.percent);
            self.statistics.data.swap.used = parseFloat(stats.swap.used);
            self.statistics.data.swap.total = parseFloat(stats.swap.total);
            
            /*
             * we send the data now and we reset
             */
            self.statistics.data.date_end = Date.now();
            
            self.emit('get:statistics', self.statistics.data);
            self.statistics.data = {
                mem: {
                    percent: 0,
                    used: 0,
                    total: 0
                },
                swap: {
                    percent: 0,
                    used: 0,
                    total: 0
                }
            }
        }
    }
};

RamManager.prototype.setPolicies = function(policies) {
    var self = this;
    self.policies = {};
    policies.forEach(function(policy) {
        if(!self.policies[policy.id]) {
            self.policies[policy.id] = policy;
        } else {
            policy['status'] = self.policies[policy.id]['status'];
            self.policies[policy.id] = policy;
        }
    });
};

RamManager.prototype.checkPolicies = function() {
    var self = this;
    
    for(var pid in self.policies) {
        if (self.ramModel.model.usage.mem.percent >= self.policies[pid]['ram_open_percent'] && (self.policies[pid]['status'] === 'closed' || self.policies[pid]['status'] === 'not_verified')) {
            //open the policy and add the incident
            self.policies[pid]['status'] = 'opened';
            var ramUsage = {'policy': self.policies[pid], 'data': self.ramModel.getModel(), 'status': 'open', type: 'ram_usage', time: Date.now()};
            self.emit('policy:send', ramUsage);
        }
        if (self.ramModel.model.usage.mem.percent < self.policies[pid]['ram_close_percent'] && (self.policies[pid]['status'] === 'opened' || self.policies[pid]['status'] === 'not_verified')) {
            //close the policy and close the incident
            self.policies[pid]['status'] = 'closed';
            var ramUsage = {'policy': self.policies[pid], 'data': self.ramModel.getModel(), 'status': 'close', type: 'ram_usage', time: Date.now()};
            self.emit('policy:send', ramUsage);
        }
        //special case when usage percent is betwen open percent and close percent and status is not_verified
        if (self.ramModel.model.usage.mem.percent < self.policies[pid]['ram_open_percent'] && (self.policies[pid]['status'] === 'not_verified')) {
            //close the policy and close the incident
            self.policies[pid]['status'] = 'closed';
            var ramUsage = {'policy': self.policies[pid], 'data': self.ramModel.getModel(), 'status': 'close', type: 'ram_usage', time: Date.now()};
            self.emit('policy:send', ramUsage);
        }
    }
};

RamManager.prototype.__proto__ = Manager.prototype;

module.exports = RamManager;