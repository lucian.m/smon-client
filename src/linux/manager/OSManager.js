var _ = require('lodash');
var fs = require('fs');
var os = require('os');

var Manager = require('../manager/Manager');
var logger = require('../../../log');

function OSManager() {
    this.os = {
        'hostname': os.hostname(),
        'loadavg': os.loadavg(),
        'uptime': os.uptime(),
        'totalmem': os.totalmem(),
        'type': os.type(),
        'release': os.release(),
        'arch': os.arch(),
        'platform': os.platform()
        };
    
    this.policies = {};
}

OSManager.prototype.getStats = function () {
    this.os = {
        'hostname': os.hostname(),
        'loadavg': os.loadavg(),
        'uptime': os.uptime(),
        'uptime_date': Date.now() - (os.uptime() * 1000),
        'totalmem': os.totalmem(),
        'type': os.type(),
        'release': os.release(),
        'arch': os.arch(),
        'platform': os.platform(),
        'cpu_model': os.cpus()[0].model,
        'time': Date.now()
        };

    this.emit('get:stats', this.os);
};

OSManager.prototype.runStats = function(time) {
    var self = this;
    var int = setInterval(interval, time);
    function interval() {
        self.getStats();
    }
    
    return int;
};

OSManager.prototype.setPolicies = function(policies) {
    var self = this;
    self.policies = {};
    policies.forEach(function(policy) {
        if(!self.policies[policy.id]) {
            self.policies[policy.id] = policy;
        } else {
            self.policies[policy.id] = policy;
            self.policies[policy.id]['status'] = self.policies[policy.id]['status'];
        }
    });
};

OSManager.prototype.__proto__ = Manager.prototype;

module.exports = OSManager;