var events = require("events");

var logger = require('../../log');

var CpuManager = require('./manager/CpuManager.js');
var RamManager = require('./manager/RamManager.js');
var DiskManager = require('./manager/DiskManager.js');
var ProcessManager = require('./manager/ProcessManager.js');
var NetworkManager = require('./manager/NetworkManager.js');
var OSManager = require('./manager/OSManager.js');

function PluginManager() {
    events.EventEmitter.call(this);
    this.CpuManager = new CpuManager();
    this.DiskManager = new DiskManager(this.CpuManager);
    this.RamManager = new RamManager();
    this.ProcessManager = new ProcessManager();
    this.NetworkManager = new NetworkManager();
    this.OSManager = new OSManager();

    this.runStats();
}

PluginManager.prototype.runStats = function() {
    var self = this;
    this.CpuManager.runStats(1000);
    this.CpuManager.on('get:stats', function(data) {
        var newData = {'plugin': 'cpu_manager', 'event': 'get:stats', 'data': data};
        self.emit('get:data', newData);
    });
    this.CpuManager.on('policy:send', function(data) {
        self.emit('policy:send', data);
    });
    this.CpuManager.on('get:statistics', function(data) {
        var newData = {'plugin': 'cpu_manager', 'event': 'get:statistics', 'data': data};
        self.emit('get:statistics', newData);
    });


    this.RamManager.runStats(1000);
    this.RamManager.on('get:stats', function(data) {
        var newData = {'plugin': 'ram_manager', 'event': 'get:stats', 'data': data};
        self.emit('get:data', newData);
    });
    this.RamManager.on('policy:send', function(data) {
        self.emit('policy:send', data);
    });
    this.RamManager.on('get:statistics', function(data) {
        var newData = {'plugin': 'ram_manager', 'event': 'get:statistics', 'data': data};
        self.emit('get:statistics', newData);
    });


    this.DiskManager.runStats(1000);
    this.DiskManager.on('get:stats', function(data) {
        var newData = {'plugin': 'disk_manager', 'event': 'get:stats', 'data': data};
        self.emit('get:data', newData);
    });
    this.DiskManager.on('get:statistics', function(data) {
        var newData = {'plugin': 'disk_manager', 'event': 'get:statistics', 'data': data};
        self.emit('get:statistics', newData);
    });

    this.ProcessManager.runStats(50000);
    this.ProcessManager.on('get:stats', function(data) {
        var newData = {'plugin': 'process_manager', 'event': 'get:stats', 'data': data};
        self.emit('get:data', newData);
    });
    this.ProcessManager.on('policy:send', function(data) {
        self.emit('policy:send', data);
    });
    this.ProcessManager.on('get:statistics', function(data) {
        var newData = {'plugin': 'process_manager', 'event': 'get:statistics', 'data': data};
        self.emit('get:statistics', newData);
    });

    this.NetworkManager.runStats(1000);
    this.NetworkManager.on('get:stats', function(data) {
        var newData = {'plugin': 'network_manager', 'event': 'get:stats', 'data': data};
        self.emit('get:data', newData);
    });
    this.NetworkManager.on('policy:send', function(data) {
        self.emit('policy:send', data);
    });
    this.NetworkManager.on('get:statistics', function(data) {
        var newData = {'plugin': 'network_manager', 'event': 'get:statistics', 'data': data};
        self.emit('get:statistics', newData);
    });

    this.OSManager.runStats(1000);
    this.OSManager.on('get:stats', function(data) {
        var newData = {'plugin': 'os_manager', 'event': 'get:stats', 'data': data};
        self.emit('get:data', newData);
    });
};

PluginManager.prototype.checkErrors = function() {
    this.CpuManager.on('error', function(data) {
        logger.log('error', data);
    });

    this.RamManager.on('error', function(data) {
        logger.log('error', data);
    });

    this.DiskManager.on('error', function(data) {
        logger.log('error', data);
    });

    this.ProcessManager.on('error', function(data) {
        logger.log('error', data);
    });

    this.NetworkManager.on('error', function(data) {
        logger.log('error', data);
    });

    this.OSManager.on('error', function(data) {
        logger.log('error', data);
    });
};

PluginManager.prototype.handlePolicies = function(data) {
    var policies = JSON.parse(data);
    for(var type in policies) {
        if(type === 'server_online') {
            this.OSManager.setPolicies(policies[type]);
        }
        if(type === 'cpu_load') {
            this.CpuManager.setPolicies(policies[type]);
        }
        if(type === 'ram_usage') {
            this.RamManager.setPolicies(policies[type]);
        }
        if(type === 'process_running') {
            this.ProcessManager.setPolicies(policies[type]);
        }
        if(type === 'adapter_download' || type === 'adapter_upload') {
            this.NetworkManager.setPolicies(policies[type], type);
        }
    }
};

PluginManager.prototype.__proto__ = events.EventEmitter.prototype;

module.exports.PluginManager = PluginManager;
