var fs = require ('fs');
var ajax_utils = require('../utils/ajax-utils.js');
var logger = require('winston');

exports.checkVersion = function(callback) {
    var url = 'https://bitbucket.org/webdilio/smon-client/raw/master/package.json';
    ajax_utils.xhrCall(url, function(response) {
        var obj = JSON.parse(fs.readFileSync(__dirname + '/../../../package.json', 'utf8'));
        var remote = JSON.parse(response);
        if(remote.version === obj.version){
            logger.info('Current version up to date.');
            callback({version: remote.version, update: false});
        } else{
          logger.info('New version '+remote.version+' Available');
          callback({version: remote.version, update: true});
        }
    });
};
