/* Global imports */
logger = require('winston');

exports.downloadFile = function (src, output, options, callback){
    var wget = require('wget');
    var download = wget.download(src, output, options);
    download.on('error', function(err) {
        logger.error('Error', err);
        callback();
    });
    download.on('end', function(output) {
        logger.info('File downloaded succesfully...');
        setTimeout(function(){
            callback(output);
        },3000);
    });
    download.on('progress', function(progress) {
        logger.info(progress);
    });
}
