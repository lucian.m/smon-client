var _ = require('lodash');
var Model = require('../model/Model');

function RamModel() {
    this.model = {
        'mem': {
            'total': 0,
            'used': 0,
            'free': 0,
            'shared': 0,
            'buffers': 0,
            'cached': 0
        },
        'swap': {
            'total': 0,
            'used': 0,
            'free': 0
        },
        'buffers': {
            'used': 0,
            'free': 0
        },
        usage: {
            'mem': {
                'percent': 0
            },
            'swap': {
                'percent': 0
            }
        }
    };
    
    this.freeVersion = 'old';
}

RamModel.prototype.update = function (str) {
    var self = this;
    var lines = str.toString().split(/\n/g);
    for (var x = 0; x < lines.length; x++) {
        var line = lines[x];
        var compontents = line.split(/ /g);
        compontents = cleanArray(compontents);
        //mem
        if (compontents[0] === 'Mem:') {
            self.model['mem']['total'] = parseInt(compontents[1]);
            self.model['mem']['used'] = parseInt(compontents[2]);
            self.model['mem']['free'] = parseInt(compontents[3]);
            //changed the free -m
            if(lines.length == 4) {
                self.model['buffers']['used'] = self.model['mem']['total'] - parseInt(compontents[6]);
                self.model['buffers']['free'] = parseInt(compontents[6]);
                self.freeVersion = 'new';
            } else {
                self.model['mem']['shared'] = parseInt(compontents[4]);
                self.model['mem']['buffers'] = parseInt(compontents[5]);
                self.model['mem']['cached'] = parseInt(compontents[6]);
            }
        }
        if (compontents[0] === 'Swap:') {
            self.model['swap']['total'] = parseInt(compontents[1]);
            self.model['swap']['used'] = parseInt(compontents[2]);
            self.model['swap']['free'] = parseInt(compontents[3]);
        }
        
        if (compontents[1] === 'buffers/cache:') {
            //changed the free -m
//            if(lines.length == 4) {
                self.model['buffers']['used'] = parseInt(compontents[2]);
                self.model['buffers']['free'] = parseInt(compontents[3]);
//            }
        }
        
    }

    this.setUsage();
};

RamModel.prototype.setUsage = function () {
    var self = this;
//    if(this.freeVersion === 'new') {
//        this.model.usage.mem.percent = _.round((self.model['mem']['total'] - self.model['buffers']['free']) / self.model['mem']['total'] * 100, 2);
//    } else {
        this.model.usage.mem.percent = _.round(self.model['buffers']['used'] / self.model['mem']['total'] * 100, 2);
//    }
    if(self.model['swap']['used'] > 0) {
        this.model.usage.swap.percent = _.round(self.model['swap']['used'] / self.model['swap']['total'] * 100, 2);
    }
};

RamModel.prototype.__proto__ = Model.prototype;

/*
 * Private functions
 */

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}

module.exports = RamModel;