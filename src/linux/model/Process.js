var _ = require('lodash');
var Model = require('../model/Model');

function ProcessModel(hash) {
    this.model = {
        'hash': hash,
        'pid': null,
        'ppid': null,
        'user': null,
        'pcpu': null,
        'pmem': null,
        'vsz': null,
        'rss': null,
        'stat': null,
        'stime': null,
        'time': null,
        'comm': null,
        'command': null,
        instances: 1
    };
}

ProcessModel.prototype.update = function (process, instance) {
    var self = this;
    if(instance) {
        self.model['instances']++;
    }
    for(var key in process) {
        if(instance && (key === 'pcpu' || key === 'pmem' || key === 'vsz' || key === 'rss')) {
            self.model[key] = parseFloat(self.model[key]) + parseFloat(process[key]);
        } else {
            self.model[key] = process[key];
        }
    }
};

ProcessModel.prototype.__proto__ = Model.prototype;

/*
 * Private functions
 */

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}

module.exports = ProcessModel;
