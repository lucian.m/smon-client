var _ = require('lodash');
var Model = require('../model/Model');

function CpuModel(id, isPrimary) {
    this.model = {
        'id': id,
        'is_primary': isPrimary,
        'proc_stat': {
            'user': 0,
            'nice': 0,
            'system': 0,
            'idle': 0,
            'iowait': 0,
            'irq': 0,
            'softirq': 0,
            'steal': 0,
            'guest': 0,
            'guest_nice': 0 
        },
        usage: {
            'percent': 0,
            'prevIdle': 0,
            'prevNonIdle': 0,
            'prevTotal': 0,
            'deltams': 0
        }
    };

}

CpuModel.prototype.updateProc = function(values) {
    var self = this;
    var prevProc = _.clone(self.model.proc_stat);
    var fields = _.keys(self.model.proc_stat);
    values.forEach(function (v, i) {
        self.model.proc_stat[fields[i]] = parseInt(v);
    });
    
    this._setUsage(prevProc);
};

CpuModel.prototype._setUsage = function(prevProc) {
    var nonIdle = this.model['proc_stat']['user'] + this.model['proc_stat']['nice'] + this.model['proc_stat']['system'] + this.model['proc_stat']['irq'] + this.model['proc_stat']['softirq'] + this.model['proc_stat']['steal'];
    var idle = this.model['proc_stat']['idle'] + this.model['proc_stat']['iowait'];
    var total = nonIdle + idle;
    var totald = total - this.model.usage.prevTotal;
    var idled = idle - this.model.usage.prevIdle;

    this.model.usage.prevTotal = total;
    this.model.usage.prevIdle = idle;
    
    this.model.usage.percent = _.round(((totald - idled)/totald * 100), 2);
    
    this.model.usage.deltams = 1000 * ((this.model['proc_stat']['user'] + this.model['proc_stat']['system']+ this.model['proc_stat']['irq'] + this.model['proc_stat']['softirq'] + this.model['proc_stat']['idle']  + this.model['proc_stat']['iowait'])
                        - (prevProc['user'] + prevProc['system'] + prevProc['irq'] + prevProc['softirq'] + prevProc['idle']  + prevProc['iowait'])) / 100 //HZ
    ;
};

CpuModel.prototype.__proto__ = Model.prototype;

module.exports = CpuModel;