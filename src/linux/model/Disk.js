var _ = require('lodash');
var Model = require('../model/Model');

function DiskModel(id, majorId, minorId) {
    this.model = {
        'id': id,
        'majorId': majorId,
        'minorId': minorId,
        'name': '',
        'kname': '',
        'label': '',
        'size': '',
        'state': '',
        'fstype': '',
        'model': '',
        'mountpoint': '',
        'type': '',
        children: {},
        'df': {
            'total': 0,
            'available': 0,
            'used': 0,
            'usedp': 0
        },
        stats: {
            'deltams': 0,
            'busy': 0,
            'rd_ios': 0,
            'wr_ios': 0,
            'rd_iops': 0,
            'wr_iops': 0,
            'iops': 0
        },
        'io': {
            "device_number": 0, "device_number_minor": 0, "device": ''
            , "reads_completed": 0, "reads_merged": 0, "sectors_read": 0, "ms_reading": 0
            , "writes_completed": 0, "writes_merged": 0, "sectors_written": 0, "ms_writing": 0
            , "ios_pending": 0, "ms_io": 0, "ms_weighted_io": 0
        }
    };
}

DiskModel.prototype.addChild = function (child) {
    var self = this;
    self.model.children[child.model.id] = child;
};

DiskModel.prototype.updateDisk = function (values) {
    var self = this;
    _.forEach(values, function (val, key) {
        self.model[key] = val;
    });
};

DiskModel.prototype.updateDS = function (values) {
    var self = this;
    self.model.df['total'] = parseInt(values['Available']) + parseInt(values['Used']);
    self.model.df['available'] = parseInt(values['Available']); // available
    self.model.df['used'] = parseInt(values['Used']);
    self.model.df['usedp'] = _.round(self.model.df['used']/self.model.df['total'] * 100, 2);
};

DiskModel.prototype.updateIO = function (values, deltams) {
    var self = this;
    var prevModel = _.clone(self.model.io);
    var fields = _.keys(self.model.io);
    values.forEach(function (v, i) {
        self.model.io[fields[i]] = v;
    });
    var newModel = {};
    this.model.stats.deltams = deltams;
    newModel['ticks'] = parseFloat(self.model.io.ms_io) - parseFloat(prevModel.ms_io);
    // this.model.stats.rd_ios = _.round((parseFloat(self.model.io.sectors_read) - parseFloat(prevModel.sectors_read)) / parseFloat(deltams) * 1024 / 2, 2);
    // this.model.stats.wr_ios = _.round((parseFloat(self.model.io.sectors_written) - parseFloat(prevModel.sectors_written)) / parseFloat(deltams) * 1024 / 2, 2);
    this.model.stats.rd_ios = _.round((parseFloat(self.model.io.sectors_read / 2) - parseFloat(prevModel.sectors_read / 2)), 2);
    this.model.stats.wr_ios = _.round((parseFloat(self.model.io.sectors_written / 2) - parseFloat(prevModel.sectors_written / 2)), 2);

    this.model.stats.rd_iops = _.round((parseFloat(self.model.io.sectors_read) - parseFloat(prevModel.sectors_read)), 2);
    this.model.stats.wr_iops = _.round((parseFloat(self.model.io.sectors_written) - parseFloat(prevModel.sectors_written)), 2);

    this.model.stats.iops = this.model.stats.rd_iops + this.model.stats.wr_iops;

    this.model.stats.busy  = _.round(parseFloat(newModel['ticks']) / parseFloat(deltams) * 100, 2);
    if(this.model.stats.busy > 100) {
        this.model.stats.busy = 100;
    }

//    if(this.model.mountpoint.indexOf('/') >= 0) {
//        console.log(this.model.name + '   ' + this.model.stats.rd_ios + '   ' + this.model.stats.wr_ios + '    ' + this.model.stats.busy + '      ' + this.model.mountpoint);
//    }
};

DiskModel.prototype.__proto__ = Model.prototype;

module.exports = DiskModel;
