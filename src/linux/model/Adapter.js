var _ = require('lodash');
var Model = require('../model/Model');

function AdapterModel(name) {
    this.model = {
        'name': name,
        'ips': [],
        'stats': {
            'total': {
                'receive': {
                    'bytes': 0,
                    'packets': 0,
                    'errs': 0,
                    'drop': 0,
                    'fifo': 0,
                    'frame': 0,
                    'compressed': 0,
                    'multicast': 0
                },
                'transmit': {
                    'bytes': 0,
                    'packets': 0,
                    'errs': 0,
                    'drop': 0,
                    'fifo': 0,
                    'colls': 0,
                    'carrier': 0,
                    'compressed': 0
                }
            },
            'live': {
                'receive': {
                    'bytes': 0,
                    'packets': 0,
                    'errs': 0,
                    'drop': 0,
                    'fifo': 0,
                    'frame': 0,
                    'compressed': 0,
                    'multicast': 0
                },
                'transmit': {
                    'bytes': 0,
                    'packets': 0,
                    'errs': 0,
                    'drop': 0,
                    'fifo': 0,
                    'colls': 0,
                    'carrier': 0,
                    'compressed': 0
                }
            }
        }
    };
};

AdapterModel.prototype.update = function (adapter) {
    var self = this;
    for(var key in adapter) {
        this.model.ips.push({'ip': adapter[key]['address'], 'family': adapter[key]['family']});
    }
};

AdapterModel.prototype.updateStats = function (line) {
    var self = this;
    var prevModel = _.clone(self.model.stats.total);
    
    self.model.stats.live = {
        'receive': {
            'bytes': (parseInt(line[1]) - prevModel['receive']['bytes']),
            'packets': parseInt(line[2]) - prevModel['receive']['packets'],
            'errs': parseInt(line[3]) - prevModel['receive']['errs'],
            'drop': parseInt(line[4]) - prevModel['receive']['drop'],
            'fifo': parseInt(line[5]) - prevModel['receive']['fifo'],
            'frame': parseInt(line[6]) - prevModel['receive']['frame'],
            'compressed': parseInt(line[7]) - prevModel['receive']['compressed'],
            'multicast': parseInt(line[8] - prevModel['receive']['multicast'])
        },
        'transmit': {
            'bytes': (parseInt(line[9]) - prevModel['transmit']['bytes']),
            'packets': parseInt(line[10]) - prevModel['transmit']['packets'],
            'errs': parseInt(line[11]) - prevModel['transmit']['errs'],
            'drop': parseInt(line[12]) - prevModel['transmit']['drop'],
            'fifo': parseInt(line[13]) - prevModel['transmit']['fifo'],
            'colls': parseInt(line[14]) - prevModel['transmit']['colls'],
            'carrier': parseInt(line[15]) - prevModel['transmit']['carrier'],
            'compressed': parseInt(line[16]) - prevModel['transmit']['compressed']
        }
    }
    
    self.model.stats.total = {
        'receive': {
            'bytes': parseInt(line[1]),
            'packets': parseInt(line[2]),
            'errs': parseInt(line[3]),
            'drop': parseInt(line[4]),
            'fifo': parseInt(line[5]),
            'frame': parseInt(line[6]),
            'compressed': parseInt(line[7]),
            'multicast': parseInt(line[8])
        },
        'transmit': {
            'bytes': parseInt(line[9]),
            'packets': parseInt(line[10]),
            'errs': parseInt(line[11]),
            'drop': parseInt(line[12]),
            'fifo': parseInt(line[13]),
            'colls': parseInt(line[14]),
            'carrier': parseInt(line[15]),
            'compressed': parseInt(line[16])
        }
    };
};

AdapterModel.prototype.__proto__ = Model.prototype;

module.exports = AdapterModel;