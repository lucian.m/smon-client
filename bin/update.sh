#!/bin/bash

TEXT_BOLD='\e[1m'
TEXT_RESET='\e[0m'

COLOR_RESET='\e[0m'
COLOR_BLUE='\e[94m'
COLOR_GREEN='\e[32m'
COLOR_RED='\e[31m'
COLOR_YELLOW='\e[93m'

#getting some commands
curl_path=`command -v curl`
wget_path=`command -v wget`

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

SMON_USER_NAME="smon-daemon"
SMON_USER_DIR="$( cd -P "$(dirname "$DIR/../../../" )" && pwd )"
SMON_ROOT_DIR="$SMON_USER_DIR/.smon"
SMON_LOCAL_DIR=$SMON_ROOT_DIR/local
SMON_DAEMON_DIR=$SMON_USER_DIR/smon
SMON_LOCAL_BIN_DIR=$SMON_LOCAL_DIR/bin
SMON_SERVER_PID=""
SMON_APIKEY=""

if [ -f "$SMON_DAEMON_DIR/pids/server.pid" ]; then
    SMON_SERVER_PID=`cat "$SMON_DAEMON_DIR/pids/server.pid"`
fi
if [ -f "$SMON_DAEMON_DIR/configs/key" ]; then
    SMON_APIKEY=`cat "$SMON_DAEMON_DIR/configs/key"`
fi

#we kill the processes
kill $(ps aux | grep 'smon-server.js' | awk '{print $2}') &>/dev/null
kill $(ps aux | grep 'smon-monitor.js' | awk '{print $2}') &>/dev/null

rm -rf "$SMON_DAEMON_DIR/configs/config.json"
rm -rf "$SMON_DAEMON_DIR/install/"*
rm -rf "$SMON_DAEMON_DIR/pids/"*
rm -rf "$SMON_DAEMON_DIR/sock"
rm -rf "$SMON_DAEMON_DIR/src"
rm -rf "$SMON_DAEMON_DIR/log.js"
rm -rf "$SMON_DAEMON_DIR/package.json"
rm -rf "$SMON_DAEMON_DIR/smon-monitor.js"
rm -rf "$SMON_DAEMON_DIR/smon-server.js"
rm -rf "$SMON_DAEMON_DIR/bin/"*
rm -rf "$SMON_DAEMON_DIR/node_modules"

download_url="https://bitbucket.org/webdilio/smon-client/get/master.tar.gz"
download_file="$SMON_DAEMON_DIR/install/smon.tar.gz"

echo -en "${COLOR_BLUE} Updating [${TEXT_BOLD}smon.com daemon${COLOR_RESET}${COLOR_BLUE}] ...${COLOR_RESET}\n"

if [ -f "$download_file" ]; then
    echo -en "${COLOR_RED} DELETING: ${COLOR_GREEN}$download_file${COLOR_RESET}\n"
    rm -rf $download_file
fi

echo -en "${COLOR_BLUE} Downloading: ${COLOR_GREEN}smon.com daemon[$download_file]${COLOR_RESET}\n"
if [ -n "$curl_path" ]; then
    curl -o $download_file $download_url
elif [ -n "$wget_path" ]; then
    wget $download_url -O $download_file
else
    echo -en "${COLOR_RED} This script requires WGET OR CURL${COLOR_RESET}\n"
    exit 1
fi

if [ -f "$download_file" ]; then
    echo -en "${COLOR_BLUE} Extracting smon.com daemon files${COLOR_RESET}\n"
    cd "$SMON_DAEMON_DIR/install"
    tar -xzf $download_file --strip-components=1
    rm "$download_file"
else
    echo -en "${COLOR_RED} Error downloading smon.com daemon files${COLOR_RESET}\n"
    exit 1
fi

cp -R "$SMON_DAEMON_DIR/install/"* "$SMON_DAEMON_DIR"
rm -rf "$SMON_DAEMON_DIR/install/"*
cd $SMON_DAEMON_DIR
echo -en "${COLOR_BLUE} Installing smon.com daemon dependencies${COLOR_RESET}\n"
$SMON_LOCAL_BIN_DIR/npm install
cd $SMON_USER_DIR

set_pid () {
	unset PID
	_PID=`head -1 "$SMON_DAEMON_DIR/pids/server.pid" 2>/dev/null`
	if [ '$_PID' ]; then
		kill -0 $_PID 2>/dev/null && PID=$_PID
	fi
}

echo "starting smon-daemon"
CNT=5
$SMON_LOCAL_BIN_DIR/node $SMON_DAEMON_DIR/smon-server.js -key $SMON_APIKEY < /dev/null &>/dev/null &

#echo $! > "$SMON_LOCAL_DIR/daemon.pid"

while [ : ]; do
	set_pid

	if [ -n "$PID" ]; then
		echo started "smon-daemon $PID"
		break
	else
		if [ $CNT -gt 0 ]; then
			sleep 1
			CNT=`expr $CNT - 1`
		else
			echo ERROR - failed to start "smon-daemon"
			break
		fi
	fi
done
