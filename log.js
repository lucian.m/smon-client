var winston = require('winston');
var config;
try {
    config = require('./configs/config_dev.json');
}
catch (e) {
    config = require('./configs/config.json');
}
//
//winston.cli();
//winston.level = 'debug';
//winston.add(winston.transports.File, { filename: __dirname + config.server.logs.logFile });
//winston.default.transports.console.timestamp = function() {
//    return Date.now();
//}
////winston.default.transports.console.formatter = function(options) {
////        // Return string will be passed to logger.
////        return options.timestamp() +' '+ options.level.toUpperCase() +' '+ (undefined !== options.message ? options.message : '') +
////          (options.meta && Object.keys(options.meta).length ? '\n\t'+ JSON.stringify(options.meta) : '' );
////      }
//module.exports = winston;

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)(),
    new (require('winston-daily-rotate-file'))({
      name: 'info-file',
      filename: __dirname + config.server.logs.logFile,
      level: 'info'
    }),
    new (require('winston-daily-rotate-file'))({
      name: 'error-file',
      filename: __dirname + config.server.logs.errFile,
      level: 'error'
    })
  ]
});

module.exports = logger;