var child_process = require('child_process')
        , fs = require("fs-extra")
        , isThere = require('is-there')
        , versionChecker = require('./src/linux/utils/version-checker')
        , fileHandler = require('./src/linux/utils/file-utils')
        , logger = require('winston')
        //, rimraf = require('rimraf')
        , server
        ;
var winston = require('winston');

var config;
try {
    config = require('./configs/config_dev.json');
}
catch (e) {
    config = require('./configs/config.json');
}

if (process.argv.indexOf("-key") != -1) {
    var apiKey = process.argv[process.argv.indexOf("-key") + 1];
} else {
    logger.error('You need to add -key');
    process.exit(0);
}

var logger = new (winston.Logger)({
  transports: [
    //new (winston.transports.Console)(),
    new (require('winston-daily-rotate-file'))({
      name: 'server-log-file',
      filename: __dirname + config.server.logs.serverLogFile,
      level: 'info'
    }),
    new (require('winston-daily-rotate-file'))({
      name: 'server-error-file',
      filename: __dirname + config.server.logs.serverErrFile,
      level: 'error'
    })
  ]
});


function Server() {
    this.serverPidFile = __dirname + config.server.pids.server;
    this.monitorPidFile = __dirname + config.server.pids.monitor;
    this.serverOnline = false;
    this.monitorOnline = false;
    var self = this;

    this.checkPids();
    process.on('SIGINT', function() {
        console.log('sig');
        if(self.monitor) {
            self.monitor.kill();
        }
        process.exit(0);
    });
}

Server.prototype = {
    monitor: null,
    files: [],
    restarting: false,
    update: false,
    watching: false,
    "checkPids": function() {
        var self = this;
        fs.readFile(this.serverPidFile, 'utf-8', function (err, content) {
            /*
             * We have another server running
             * we try to kill the process and remove the pid file
             */
            if(content) {
                logger.info('server allready started', {'pid': content});
                self.serverOnline = content;
            }
            fs.readFile(self.monitorPidFile, 'utf-8', function (err, content) {
                if(content) {
                    logger.info('monitor allready started', {'pid': content});
                    self.monitorOnline = content;
                }
                var pids = [];
                if(self.serverOnline) {
                    pids.push(self.serverOnline);
                    fs.unlinkSync(self.serverPidFile);
                }
                if(self.monitorOnline) {
                    pids.push(self.monitorOnline);
                    fs.unlinkSync(self.monitorPidFile);
                }
                /*
                 * we kill the pids
                 */
                if(pids.length > 0) {
                    child_process.spawn('kill', ['-9', pids]);
                }
                fs.writeFile(self.serverPidFile, process.pid, function(err) {
                  if(err) {
                    logger.error('Can not write to server pid file', {'err': err});
                  }
                });
                self.start();
            });
        });
    },
    "restart": function () {
        this.restarting = true;
        logger.info('Stopping server for restart');
        if (this.monitor) {
            this.monitor.kill();
        }
        this.start();
    },
    "start": function () {
        var that = this;
        if (that.update === true) {
            var output = './master.zip';
            var dir = './install';
            that.update === false;
            setTimeout(function () {
                installUpdate(output, dir);
            }, 2000);
        } else {
            if (!that.watching) {
                //that.watchFile();
            }
            versionChecker.checkVersion(function (data) {
                if (data.update) {
                    logger.info('Updating...');
                    var out = fs.openSync(__dirname+'/logs/update_out.log', 'a');
                    var err = fs.openSync(__dirname+'/logs/update_err.log', 'a');
                    var updater = child_process.spawn(__dirname + '/bin/update.sh', [], {'detached': true, 'stdio': ['ignore', out, err], 'shell': true});
                    updater.unref();
                 } else {
                    logger.info('Starting server');
                    // fs.readFile(__dirname + config.smon_server.apiFile, 'utf-8', function (err, apiKey) {
                    //     if(err) {
                    //         logger.error('error reading api key file', err)
                    //     } else {
                            that.monitor = child_process.spawn(process.argv[0], [__dirname + '/smon-monitor.js', '-key', apiKey]);

                            fs.writeFile(that.monitorPidFile, that.monitor.pid, function(err) {
                                if(err) {
                                    logger.error('cannot write monitor pid file', {'file': that.monitorPidFile, 'pid': that.monitor.pid});
                                    that.monitor.kill();
                                    that.start();
                                }
                            });

                            that.monitor.stdin.addListener('data', function (data) {
        //                        process.stdout.write(data);
                            });

                            that.monitor.stdout.addListener('data', function (data) {
        //                        process.stdout.write(data);
                            });

                            that.monitor.stderr.addListener('data', function (data) {
                                logger.error(data.toString());
                            });

                            that.monitor.addListener('exit', function (code) {
                                that.monitor = null;
                                logger.info('exit', code);

                                that.restarting === true;
                                setTimeout(function () {
                                    that.start();
                                }, 5000);

                            });
                        // }
                    // });

                }
            });
        }
    },
    "watchFile": function () {
        this.watching = true;
        var that = this;
        fs.watchFile(__dirname + '/configs/config_dev.json', {interval: 500}, function (curr, prev) {
            if (curr.mtime.valueOf() != prev.mtime.valueOf() || curr.ctime.valueOf() != prev.ctime.valueOf()) {
                logger.info('Restarting because of changed file');
                // give browser time to load finish page
                setTimeout(function () {
                    that.restart();
                }, 2000);
            }
        });
        fs.watchFile(__dirname + '/configs/update.js', {interval: 500}, function (curr, prev) {
            if (curr.mtime.valueOf() != prev.mtime.valueOf() || curr.ctime.valueOf() != prev.ctime.valueOf()) {
                logger.info('Restarting because an update is available');
                that.update = true;
                that.restart();
            }
        });
    }
}

var server = new Server();
